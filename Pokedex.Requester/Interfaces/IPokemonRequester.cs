﻿using System.Threading.Tasks;
using Pokedex.Requester.Models;

namespace Pokedex.Requester.Interfaces
{
    public interface IPokemonRequester
    {
        Task<PokemonDetailsResult> Get(string uri);
        Task<PokemonsResult> GetAll(string uri = "https://pokeapi.co/api/v2/pokemon");
    }
}