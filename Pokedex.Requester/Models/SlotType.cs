﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex.Requester.Models
{
    public class SlotType
    {
        public int Slot { get; set; }
        public Type Type { get; set; }
    }
}
