﻿using Pokedex.Requester.Interfaces;
using Pokedex.Requester.Models;
using System.Net.Http;
using System.Threading.Tasks;
using ToolBox.Requester;

namespace Pokedex.Requester.Requesters
{
    public class PokemonRequester : RequesterBase, IPokemonRequester
    {
        public PokemonRequester(HttpClient client) : base(client)
        {
        }

        public Task<PokemonsResult> GetAll(string uri = "https://pokeapi.co/api/v2/pokemon")
        {
            return GetAsync<PokemonsResult>(uri);
        }

        public Task<PokemonDetailsResult> Get(string uri)
        {
            return GetAsync<PokemonDetailsResult>(uri);
        }
    }
}
