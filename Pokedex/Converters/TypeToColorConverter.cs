﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Pokedex.Converters
{
    public class TypeToColorConverter : IValueConverter
    {
        private Dictionary<string, SolidColorBrush> _Pairs;

        public TypeToColorConverter()
        {
            _Pairs = new Dictionary<string, SolidColorBrush>
            {
                { "normal" , new SolidColorBrush(Color.FromRgb(170,170,153)) },
                { "fire" , new SolidColorBrush(Color.FromRgb(255,68,34)) },
                { "water" , new SolidColorBrush(Color.FromRgb(51,153,255)) },
                { "electric" , new SolidColorBrush(Color.FromRgb(255,204,51)) },
                { "grass" , new SolidColorBrush(Color.FromRgb(119,204,85)) },
                { "ice" , new SolidColorBrush(Color.FromRgb(102,204,255)) },
                { "fighting" , new SolidColorBrush(Color.FromRgb(187,85,68)) },
                { "poison" , new SolidColorBrush(Color.FromRgb(170,85,153)) },
                { "ground" , new SolidColorBrush(Color.FromRgb(221,187,85)) },
                { "flying" , new SolidColorBrush(Color.FromRgb(136,153,255)) },
                { "psychic" , new SolidColorBrush(Color.FromRgb(255,85,153)) },
                { "bug" , new SolidColorBrush(Color.FromRgb(170,187,34)) },
                { "rock" , new SolidColorBrush(Color.FromRgb(187,170,102)) },
                { "ghost" , new SolidColorBrush(Color.FromRgb(102,102,187)) },
                { "dragon" , new SolidColorBrush(Color.FromRgb(119,102,238)) },
                { "dark" , new SolidColorBrush(Color.FromRgb(119,85,68)) },
                { "steel" , new SolidColorBrush(Color.FromRgb(170,170,187)) },
                { "fairy" , new SolidColorBrush(Color.FromRgb(238,153,238)) }
            };
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            return _Pairs[value.ToString()];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
