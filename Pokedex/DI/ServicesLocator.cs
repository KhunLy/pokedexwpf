﻿using Microsoft.Extensions.DependencyInjection;
using Pokedex.Requester.Interfaces;
using Pokedex.Requester.Requesters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex.DI
{
    class ServicesLocator
    {
        private readonly static object _The_lock = new object();

        private static ServicesLocator _Instance;

        public static ServicesLocator Instance
        {
            get { lock (_The_lock) { return _Instance = _Instance ?? new ServicesLocator(); } }
        }

        private readonly IServiceCollection _Collection;

        private ServiceProvider _Provider;
        private ServiceProvider Provider
        {
            get { return _Provider = _Provider ?? _Collection.BuildServiceProvider(); }
        }

        private ServicesLocator()
        {
            _Collection = new ServiceCollection();
            _Collection.AddTransient<HttpClient>();
            _Collection.AddTransient<IPokemonRequester, PokemonRequester>();
        }

        public T Get<T>()
        {
            return (T)Provider.GetService(typeof(T));
        } 
    }
}
