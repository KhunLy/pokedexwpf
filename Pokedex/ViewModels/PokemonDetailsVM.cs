﻿using Pokedex.DI;
using Pokedex.Requester.Interfaces;
using Pokedex.Requester.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BasesClasses;
using ToolBox.MVVM.Mappers;
using ToolBox.MVVM.Mediators;

namespace Pokedex.ViewModels
{
    public class PokemonDetailsVM : BindableBase
    {
        private string _Name;

        public string Name
        {
            get { return _Name ?? "Unknown"; }
            set { _Name = value; RaisePropertyChanged(); }
        }

        private double _Height;

        public double Height
        {
            get { return _Height / 10d; }
            set { _Height = value; RaisePropertyChanged(); }
        }

        private double _Weight;

        public double Weight
        {
            get { return _Weight / 10d; }
            set { _Weight = value; RaisePropertyChanged(); }
        }

        private string _FrontDefault;

        public string FrontDefault
        {
            get { return _FrontDefault; }
            set { _FrontDefault = value; RaisePropertyChanged(); }
        }

        private string _BackDefault;

        public string BackDefault
        {
            get { return _BackDefault; }
            set { _BackDefault = value; RaisePropertyChanged(); }
        }

        private string _Type1;

        public string Type1
        {
            get { return _Type1; }
            set { _Type1 = value; RaisePropertyChanged(); }
        }

        private string _Type2;

        public string Type2
        {
            get { return _Type2; }
            set { _Type2 = value; RaisePropertyChanged(); }
        }


        public PokemonDetailsVM()
        {
            Messenger<string>.Subscribe("DETAILS_CHANGED", OnDetailsChanged);
        }

        public void OnDetailsChanged(string url)
        {
            ServicesLocator.Instance.Get<IPokemonRequester>()
                .Get(url).ContinueWith(RefreshAsync);
        }

        public async void RefreshAsync(Task<PokemonDetailsResult> task)
        {
            PokemonDetailsResult result = await task;
            Name = result.Name;
            Height = result.Height;
            Weight = result.Weight;
            FrontDefault = result.Sprites.FrontDefault;
            BackDefault = result.Sprites.BackDefault;
            Type1 = result.Types.FirstOrDefault(t => t.Slot == 1).Type.Name;
            Type2 = result.Types.FirstOrDefault(t => t.Slot == 2)?.Type.Name;
        }
    }
}
