﻿using Pokedex.DI;
using Pokedex.Requester.Interfaces;
using Pokedex.Requester.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using ToolBox.MVVM.BasesClasses;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace Pokedex.ViewModels
{
    public class PokemonMasterVM : BindableBase
    {
        private string _Next;

        public string Next
        {
            get { return _Next; }
            set { _Next = value; App.Current.Dispatcher.Invoke(NextCommand.RaiseCanExecuteChanged); }
        }

        private string _Previous;

        public string Previous
        {
            get { return _Previous; }
            set { _Previous = value; App.Current.Dispatcher.Invoke(PreviousCommand.RaiseCanExecuteChanged); }
        }

        private PokemonResult _SelectedItem;
        public PokemonResult SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if (_SelectedItem != value && value != null)
                {
                    _SelectedItem = value;
                    Messenger<string>.Publish("DETAILS_CHANGED", _SelectedItem?.Url);
                }
            }
        }

        private ObservableCollection<PokemonResult> _Pokemons;

        public ObservableCollection<PokemonResult> Pokemons
        {
            get { return _Pokemons; }
            private set { _Pokemons = value; RaisePropertyChanged(); }
        }

        public RelayCommand NextCommand { get; private set; }
        public RelayCommand PreviousCommand { get; private set; }

        public PokemonMasterVM()
        {
            ServicesLocator.Instance.Get<IPokemonRequester>().GetAll().ContinueWith(RefreshAsync);
            NextCommand = new RelayCommand(ChangeNext, () => Next != null);
            PreviousCommand = new RelayCommand(ChangePrev, () => Previous != null);
        }

        private void ChangeNext()
        {
            ServicesLocator.Instance.Get<IPokemonRequester>().GetAll(Next)
                .ContinueWith(RefreshAsync);
            Next = null;
            Previous = null;
        }

        private void ChangePrev()
        {
            ServicesLocator.Instance.Get<IPokemonRequester>().GetAll(Previous)
                .ContinueWith(RefreshAsync);
            Previous = null;
            Previous = null;
        }

        private async void RefreshAsync(Task<PokemonsResult> task)
        {
            PokemonsResult result = await task;
            Next = result.Next;
            Previous = result.Previous;
            Pokemons = new ObservableCollection<PokemonResult>(result.Results);
        }

    }
}
