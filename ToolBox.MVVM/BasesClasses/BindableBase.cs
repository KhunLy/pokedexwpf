﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;

namespace ToolBox.MVVM.BasesClasses
{
    public abstract class BindableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void RaiseAllPropertiesChanged()
        {
            GetType().GetProperties()
                .ToList()
                .ForEach(item => RaisePropertyChanged(item.Name));
        }
    }
}
