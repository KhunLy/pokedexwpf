﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class RelayCommand : RelayCommandBase
    {
        private readonly Action _Execute;

        public RelayCommand(Action execute, Func<bool> canExecute = null) : base(canExecute)
        {
            if (execute == null) throw new ArgumentException();
            _Execute = execute;
        }

        public override void Execute(object parameter)
        {
            _Execute();
        }
    }
}
