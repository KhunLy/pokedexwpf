﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public abstract class RelayCommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Func<bool> _CanExecute;

        public RelayCommandBase(Func<bool> canExecute)
        {
            _CanExecute = canExecute;
        }

        public virtual bool CanExecute(object parameter)
        {
            return _CanExecute?.Invoke() ?? true;
        }

        public abstract void Execute(object parameter);

        public virtual void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, null);
        }
    }
}
