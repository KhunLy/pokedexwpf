﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Mappers
{
    public static class MapperExtension
    {
        public static TOut Map<TOut>(this object o, params object[] ctorParameters)
        {
            ConstructorInfo ctor = typeof(TOut)
                .GetConstructor(ctorParameters.Select(p => p.GetType()).ToArray());
            if (ctor == null)
            {
                throw new ArgumentException("Invalid Constructor Parameters");
            }
            TOut result = (TOut)ctor.Invoke(ctorParameters);
            foreach (PropertyInfo propIn in o.GetType().GetProperties().Where(prop => prop.CanRead))
            {
                PropertyInfo propOut = typeof(TOut).GetProperty(propIn.Name);
                if(propOut != null && propOut.CanWrite && propOut.PropertyType.IsAssignableFrom(propIn.PropertyType))
                {
                    try
                    {
                        propOut.SetValue(result, propIn.GetValue(o));
                    }
                    catch (Exception) { } 
                }
            }
            return result;
        }
    }
}
