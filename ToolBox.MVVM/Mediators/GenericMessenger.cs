﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Mediators
{
    public static class Messenger<TParameter>
    {
        private static readonly Dictionary<string, Action<TParameter>> _Topics;

        static Messenger()
        {
            _Topics = new Dictionary<string, Action<TParameter>>();
        }

        public static void Publish(string topic, TParameter parameter)
        {
            _Topics[topic]?.Invoke(parameter);
        }

        public static void Subscribe(string topic, Action<TParameter> action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (!_Topics.ContainsKey(topic))
            {
                _Topics[topic] = action;
            }
            else
            {
                _Topics[topic] += action;
            }
        }

        public static void Unsubscribe(string topic, Action<TParameter> action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (_Topics.ContainsKey(topic))
            {
                _Topics[topic] -= action;
            }
        }
    }
}
